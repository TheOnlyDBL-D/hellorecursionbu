import static org.junit.Assert;

public class Factorial {
	
	public static int factorialR(int x){
		// Base case if x is negative?
		if(x == 0){	
			return 1;
		}
		// Recursive case
		return x*factorialR(x -1);
	}
	public int factorialI(int x){
		// Loop invariant: is something that is always constant
		// factorial = counter! for the loop invariant to be preserved
		
		int factorial = 1;
		int counter = 1;
		
		while(counter < x){
			counter++;
			factorial *= counter;
		}
		return factorial;
		// counter is going to be equal to x
		// factorial = x! and factorial = counter!
	}
	public static void main(String args[]){
		
//		System.out.println("Hello BU!");
//		System.out.println(Factorial.factorialR(5));
//		// create instance because non-static
//		System.out.println(instance.factorialI(5));
		
		Factorial instance = new Factorial();
	}
	
}
